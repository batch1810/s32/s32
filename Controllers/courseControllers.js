const Course = require("../models/Course");
const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// Create a new Course
/*
	Steps:
		1. Create a newcourse object using the mongoose model and the information from the request body
		2. Save the new user t the database
*/
module.exports.addCourse = (reqBody) =>{
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		slots: reqBody.slots
	})

	return newCourse.save().then((course, error) =>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

// Retrieve all courses
module.exports.getAllCourses =() =>{
	return Course.find({}).then(result => result)
}

// Retrieve all active courses
module.exports.getAllActive =() =>{
	return Course.find({isActive: true}).then(result => result)
}
// Retrieve specific courses
module.exports.getCourse =(courseId) =>{
	return Course.findById(courseId).then(result => result)
}

// Update a course
/*
	Steps:
	1. Create a variable "updatedCourse" which will contain the information retrieved from the request body.
	2. Find and update the course using the courseId retrieved from request params/url property and the variable "updatedCourse" containing  the information from the request body.

*/
module.exports.updateCourse = (courseId, reqBody) => {
	// Specify the fields or properties to be updated
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		slots:reqBody.slots
	}
	
	return Course.findByIdAndUpdate(courseId, reqBody).then((courseUpdate, err)=>{
		if(err){
			return false;
		}
		else{
			return true;
		}
	})
	
}

module.exports.archiveCourse = (courseId) => {
	let archivedCourse = {
		isActive: false
	}
	return Course.findByIdAndUpdate(courseId, archivedCourse).then((courseUpdate, err)=>{
		if(err){
			return false;
		}
		else{
			return true;
		}
	})
}