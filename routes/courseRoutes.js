const express = require("express");
const router = express.Router();
const courseControllers = require("../Controllers/courseControllers");
const auth = require("../auth");


// Route for creating a course

router.post("/", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization); //contains the token 
	if (userData.isAdmin) {
		courseControllers.addCourse(req.body).then(resultFromController => res.send(resultFromController));
	}
	// 
	else {
		res.send("You don’t have permission on this page!");
	}
})

// Route for retrieving all the courseControllers
router.get("/all", auth.verify, (req, res) =>{
	courseControllers.getAllCourses().then(resultFromController => res.send(resultFromController));

})

// Route for retrieving all the active courseControllers
router.get("/", (req, res) =>{

	courseControllers.getAllActive().then(resultFromController => res.send(resultFromController));

})

// Route for retrieving specific course
router.get("/:courseId", (req, res) =>{
	console.log(req.params.courseId)
	courseControllers.getCourse(req.params.courseId).then(resultFromController => res.send(resultFromController));

})

// Router for Updating Course
router.put("/:courseId", auth.verify, (req, res) =>{
	courseControllers.updateCourse(req.params.courseId, req.body).then(resultFromController => res.send(resultFromController));
})
// Route to Archive a course

router.patch("/:courseId/archive", auth.verify,(req,res)=>{
	courseControllers.archiveCourse(req.params.courseId).then(resultFromController => res.send(resultFromController))
})
module.exports = router;
